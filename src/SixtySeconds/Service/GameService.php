<?php

declare(strict_types=1);

namespace SixtySeconds\Service;

use SixtySeconds\Game;
use SixtySeconds\Util\Timer;

class GameService
{
    /**
     * @var BoardService
     */
    private $boardService;
    /**
     * @var Timer
     */
    private $timer;

    public function __construct(BoardService $boardService, Timer $timer)
    {
        $this->boardService = $boardService;
        $this->timer = $timer;
    }

    public function startNewGame(): Game
    {
        $startTime = $this->timer->measure();
        $board = $this->boardService->generate();

        return new Game($board, $startTime);
    }

    public function checkToken(Game $game, int $x, int $y): void
    {
        $game->check($x, $y, $this->timer->measure());
    }
}