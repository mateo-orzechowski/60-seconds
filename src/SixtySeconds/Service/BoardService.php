<?php

namespace SixtySeconds\Service;

use SixtySeconds\Board;
use SixtySeconds\Util\RandomGenerator;

class BoardService
{
    private const BOARD_EDGE_LENGTH = 5;

    /**
     * @var RandomGenerator
     */
    private $randomGenerator;

    public function __construct(RandomGenerator $randomGenerator)
    {
        $this->randomGenerator = $randomGenerator;
    }

    public function generate(): Board
    {
        $winningTokenCoordinates = [
            $this->randomGenerator->rand(0, self::BOARD_EDGE_LENGTH - 1),
            $this->randomGenerator->rand(0, self::BOARD_EDGE_LENGTH - 1)
        ];

        return new Board(self::BOARD_EDGE_LENGTH, $winningTokenCoordinates);
    }
}
