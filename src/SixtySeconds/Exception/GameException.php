<?php

declare(strict_types=1);

namespace SixtySeconds\Exception;

use DomainException;

class GameException extends DomainException
{
    private function __construct($message = "", $code = 0)
    {
        parent::__construct($message, $code);
    }

    public static function alreadyFinished(): self
    {
        return new self('Game already finished');
    }

    public static function tokenAlreadyRevealed(): self
    {
        return new self('Token already revealed');
    }
}