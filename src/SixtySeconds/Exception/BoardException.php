<?php

declare(strict_types=1);

namespace SixtySeconds\Exception;

use DomainException;

class BoardException extends DomainException
{
    private function __construct($message = "", $code = 0)
    {
        parent::__construct($message, $code);
    }

    public static function tokenNotExists(): self
    {
        return new self('Token for given coordinates does not exist');
    }

    public static function winningTokenCoordinatesExitsBoardSize(): self
    {
        return new self('Winning token coordinates must be within board');
    }

    public static function edgeLengthLessThenOne(): self
    {
        return new self('Board edge length cannot be less then one');
    }
}