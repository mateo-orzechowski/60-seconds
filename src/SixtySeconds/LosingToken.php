<?php

declare(strict_types=1);

namespace SixtySeconds;

class LosingToken implements Token
{
    /**
     * @var bool
     */
    private $revealed = false;

    public function isWinning(): bool
    {
        return false;
    }

    public function isRevealed(): bool
    {
        return $this->revealed;
    }

    public function reveal(): void
    {
        $this->revealed = true;
    }
}
