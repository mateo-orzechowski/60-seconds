<?php

declare(strict_types=1);

namespace SixtySeconds;

interface Token
{
    public function isWinning(): bool;

    public function isRevealed(): bool;

    public function reveal(): void;
}
