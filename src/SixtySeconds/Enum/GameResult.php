<?php

declare(strict_types=1);

namespace SixtySeconds\Enum;

class GameResult
{
    public const PENDING = 'pending';
    public const WIN = 'win';
    public const LOSE = 'lose';
}