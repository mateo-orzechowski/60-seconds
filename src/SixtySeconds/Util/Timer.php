<?php

declare(strict_types=1);

namespace SixtySeconds\Util;

class Timer
{
    public function measure(): int
    {
        return time();
    }
}