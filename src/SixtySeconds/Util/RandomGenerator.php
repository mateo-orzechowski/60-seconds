<?php

declare(strict_types=1);

namespace SixtySeconds\Util;

class RandomGenerator
{
    public function rand(int $min, int $max): int
    {
        return rand($min, $max);
    }
}