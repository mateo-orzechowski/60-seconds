<?php

declare(strict_types=1);

namespace SixtySeconds;

use SixtySeconds\Enum\GameResult;
use SixtySeconds\Exception\GameException;

class Game
{
    private const MAX_TRIES = 5;

    /**
     * @var string
     */
    private $gameResult = GameResult::PENDING;

    /**
     * @var int
     */
    private $tries = 0;

    /**
     * @var Board
     */
    private $board;

    /**
     * @var int
     */
    private $startTime;

    public function __construct(Board $board, int $startTime)
    {
        $this->board = $board;
        $this->startTime = $startTime;
    }

    public function getResult(): string
    {
        return $this->gameResult;
    }

    public function getTries(): int
    {
        return $this->tries;
    }

    public function getStartTime(): int
    {
        return $this->startTime;
    }

    public function check(int $x, int $y, int $checkTime): void
    {
        if (!$this->isPending()) {
            throw GameException::alreadyFinished();
        }

        if ($checkTime - $this->startTime > 60) {
            $this->gameResult = GameResult::LOSE;

            return;
        }

        $token = $this->board->getToken($x, $y);

        if ($token->isRevealed()) {
            throw GameException::tokenAlreadyRevealed();
        }

        $this->tries++;

        $token->reveal();

        if ($token->isWinning()) {
            $this->gameResult = GameResult::WIN;

            return;
        }

        if ($this->tries === self::MAX_TRIES) {
            $this->gameResult = GameResult::LOSE;
        }
    }

    private function isPending(): bool
    {
        return $this->gameResult === GameResult::PENDING;
    }
}
