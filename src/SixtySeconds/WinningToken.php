<?php

declare(strict_types=1);

namespace SixtySeconds;

class WinningToken implements Token
{
    /**
     * @var bool
     */
    private $revealed = false;

    public function isWinning(): bool
    {
        return true;
    }

    public function isRevealed(): bool
    {
        return $this->revealed;
    }

    public function reveal(): void
    {
        $this->revealed = true;
    }
}
