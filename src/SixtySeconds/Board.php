<?php

declare(strict_types=1);

namespace SixtySeconds;

use SixtySeconds\Exception\BoardException;

class Board
{
    /**
     * @var Token[][]
     */
    private $tokens;

    /**
     * @var int[]
     */
    private $winningTokenCoordinates;

    /**
     * @var int
     */
    private $edgeLength;

    /**
     * @param int $edgeLength
     * @param int[] $winningTokenCoordinates
     */
    public function __construct(int $edgeLength, array $winningTokenCoordinates)
    {
        $this->edgeLength = $edgeLength;
        $this->winningTokenCoordinates = $winningTokenCoordinates;

        $this->initBoard();
    }

    public function getToken(int $x, int $y): Token
    {
        $token = $this->tokens[$x][$y] ?? null;

        if (!$token) {
            throw BoardException::tokenNotExists();
        }

        return $token;
    }

    /**
     * @return Token[][]
     */
    public function getTokens(): array
    {
        return $this->tokens;
    }

    /**
     * @return int[]
     */
    public function getWinningTokenCoordinates(): array
    {
        return $this->winningTokenCoordinates;
    }

    public function getEdgeLength(): int
    {
        return $this->edgeLength;
    }

    private function initBoard(): void
    {
        $wtc = $this->winningTokenCoordinates;

        if ($this->edgeLength < 1) {
            throw BoardException::edgeLengthLessThenOne();
        }

        if ($wtc[0] >= $this->edgeLength || $wtc[1] >= $this->edgeLength) {
            throw BoardException::winningTokenCoordinatesExitsBoardSize();
        }

        for ($row = 0; $row < $this->edgeLength; $row++) {
            for ($column = 0; $column < $this->edgeLength; $column++) {
                $this->tokens[$row][$column] = new LosingToken();
            }
        }

        $this->tokens[$wtc[0]][$wtc[1]] = new WinningToken();
    }
}
