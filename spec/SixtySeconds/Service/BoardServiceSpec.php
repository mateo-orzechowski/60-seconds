<?php

namespace spec\SixtySeconds\Service;

use SixtySeconds\Service\BoardService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SixtySeconds\Util\RandomGenerator;

class BoardServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(BoardService::class);
    }

    function let(RandomGenerator $randomGenerator)
    {
        $this->beConstructedWith($randomGenerator);
    }

    function it_generates_board(RandomGenerator $randomGenerator)
    {
        // GIVEN
        $randomGenerator->rand(0, 4)->willReturn(3, 4);

        // WHEN
        $board = $this->generate();

        // THEN
        $board->getEdgeLength()->shouldBe(5);
        $board->getWinningTokenCoordinates()->shouldBe([3, 4]);
    }
}
