<?php

namespace spec\SixtySeconds\Service;

use SixtySeconds\Board;
use SixtySeconds\Enum\GameResult;
use SixtySeconds\Game;
use SixtySeconds\Service\BoardService;
use SixtySeconds\Service\GameService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SixtySeconds\Util\Timer;

class GameServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(GameService::class);
    }

    function let(BoardService $boardService, Timer $timer)
    {
        $this->beConstructedWith($boardService, $timer);
    }

    function it_generates_new_game(BoardService $boardService, Timer $timer)
    {
        // GIVEN
        $boardService->generate()->willReturn($board = $this->getBoard());
        $timer->measure()->willReturn(0);

        // WHEN
        $game = $this->startNewGame();

        // THEN
        $game->getResult()->shouldBe(GameResult::PENDING);
        $game->getTries()->shouldBe(0);
        $game->getStartTime()->shouldBe(0);
    }

    function it_checks_token_for_given_coordinates(Timer $timer, Game $game)
    {
        // GIVEN
        $timer->measure()->willReturn(10);

        // WHEN
        $this->checkToken($game, 0, 0);

        // THEN
        $game->check(0, 0, 10)->shouldHaveBeenCalled();
    }

    private function getBoard(): Board
    {
        return new Board(5, [3, 4]);
    }

    private function getGame(): Game
    {
        return new Game($this->getBoard(), 0);
    }
}
