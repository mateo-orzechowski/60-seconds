<?php

namespace spec\SixtySeconds;

use SixtySeconds\Board;
use PhpSpec\ObjectBehavior;
use SixtySeconds\Exception\BoardException;
use SixtySeconds\LosingToken;
use SixtySeconds\Token;
use SixtySeconds\WinningToken;

class BoardSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Board::class);
    }

    function let()
    {
        $this->beConstructedWith(3, [0, 0]);
    }

    function it_returns_winning_token_coordinates()
    {
        // WHEN
        $this->getWinningTokenCoordinates()

        // THEN
           ->shouldReturn([0, 0]);
    }

    function it_returns_board_edge_length()
    {
        // WHEN
        $this->getEdgeLength()

        // THEN
            ->shouldReturn(3);
    }

    function it_returns_all_tokens()
    {
        // GIVEN
        $tokens = [
            [new WinningToken(), new LosingToken(), new LosingToken()],
            [ new LosingToken(), new LosingToken(), new LosingToken()],
            [ new LosingToken(), new LosingToken(), new LosingToken()],
        ];

        // WHEN
        $this->getTokens()

        // THEN
            ->shouldBeLike($tokens);
    }

    function it_returns_token_for_given_coordinates()
    {
        // WHEN
        $this->getToken(2, 2)

        // THEN
            ->shouldBeAnInstanceOf(Token::class);
    }

    function it_throws_exception_when_token_does_not_exist()
    {
        // THEN
        $this->shouldThrow(BoardException::tokenNotExists())

        // WHEN
            ->duringGetToken(4, 4);
    }

    function it_throws_exception_when_constructed_with_winning_token_exiting_board_edge_length()
    {
        // GIVEN
        $this->beConstructedWith(1, [1, 1]);

        // THEN
        $this->shouldThrow(BoardException::winningTokenCoordinatesExitsBoardSize())

        // WHEN
            ->duringInstantiation();
    }

    function it_throws_exception_when_constructed_with_edge_length_less_then_one()
    {
        // GIVEN
        $this->beConstructedWith(0, [1, 1]);

        // THEN
        $this->shouldThrow(BoardException::edgeLengthLessThenOne())

        // WHEN
            ->duringInstantiation();
    }
}
