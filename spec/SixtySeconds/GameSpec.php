<?php

namespace spec\SixtySeconds;

use SixtySeconds\Board;
use SixtySeconds\Enum\GameResult;
use SixtySeconds\Exception\GameException;
use SixtySeconds\Game;
use PhpSpec\ObjectBehavior;
use SixtySeconds\Token;

class GameSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Game::class);
    }

    function let(Board $board)
    {
        $this->beConstructedWith($board, 0);
    }

    function it_returns_game_result()
    {
        $this->getResult()->shouldBe(GameResult::PENDING);
    }

    function it_returns_tries()
    {
        $this->getTries()->shouldBe(0);
    }

    function it_marks_token_as_revealed_when_checked(Board $board, Token $token)
    {
        // GIVEN
        $board->getToken(1, 1)->willReturn($token);
        $token->isRevealed()->willReturn(false);
        $token->isWinning()->willReturn(true);

        // THEN
        $token->reveal()->shouldBeCalled();

        // WHEN
        $this->check(1, 1, 0);
    }

    function it_increments_tries_when_token_checked(Board $board, Token $token)
    {
        // GIVEN
        $board->getToken(1, 1)->willReturn($token);
        $token->isRevealed()->willReturn(false);
        $token->reveal()->shouldBeCalled();
        $token->isWinning()->willReturn(true);
        $this->getTries()->shouldBe(0);

        // WHEN
        $this->check(1, 1, 0);

        // THEN
        $this->getTries()->shouldBe(1);
    }

    function it_marks_game_with_win_game_result_when_winning_token_revealed(Board $board, Token $token)
    {
        // GIVEN
        $board->getToken(1, 1)->willReturn($token);
        $token->isRevealed()->willReturn(false);
        $token->reveal()->shouldBeCalled();
        $token->isWinning()->willReturn(true);

        // WHEN
        $this->check(1, 1, 0);

        // THEN
        $this->getResult()->shouldBe(GameResult::WIN);
    }

    function it_marks_game_with_lose_game_result_when_player_for_the_fifth_time_revealed_losing_token(
        Board $board,
        Token $token1,
        Token $token2,
        Token $token3,
        Token $token4,
        Token $token5
    ) {
        // GIVEN
        $board->getToken(1, 1)->willReturn($token1);
        $board->getToken(1, 2)->willReturn($token2);
        $board->getToken(1, 3)->willReturn($token3);
        $board->getToken(1, 4)->willReturn($token4);
        $board->getToken(1, 5)->willReturn($token5);

        $token1->isRevealed()->willReturn(false);
        $token2->isRevealed()->willReturn(false);
        $token3->isRevealed()->willReturn(false);
        $token4->isRevealed()->willReturn(false);
        $token5->isRevealed()->willReturn(false);

        $token1->reveal()->shouldBeCalled();
        $token2->reveal()->shouldBeCalled();
        $token3->reveal()->shouldBeCalled();
        $token4->reveal()->shouldBeCalled();
        $token5->reveal()->shouldBeCalled();

        $token1->isWinning()->willReturn(false);
        $token2->isWinning()->willReturn(false);
        $token3->isWinning()->willReturn(false);
        $token4->isWinning()->willReturn(false);
        $token5->isWinning()->willReturn(false);

        $this->check(1, 1, 0);
        $this->check(1, 2, 0);
        $this->check(1, 3, 0);
        $this->check(1, 4, 0);

        // WHEN
        $this->check(1, 5, 0);

        // THEN
        $this->getResult()->shouldBe(GameResult::LOSE);
    }

    function it_throws_game_finished_exception_when_checking_after_losing(
        Board $board,
        Token $token1,
        Token $token2,
        Token $token3,
        Token $token4,
        Token $token5
    ) {

        // GIVEN
        $board->getToken(1, 1)->willReturn($token1);
        $board->getToken(1, 2)->willReturn($token2);
        $board->getToken(1, 3)->willReturn($token3);
        $board->getToken(1, 4)->willReturn($token4);
        $board->getToken(1, 5)->willReturn($token5);

        $token1->isRevealed()->willReturn(false);
        $token2->isRevealed()->willReturn(false);
        $token3->isRevealed()->willReturn(false);
        $token4->isRevealed()->willReturn(false);
        $token5->isRevealed()->willReturn(false);

        $token1->reveal()->shouldBeCalled();
        $token2->reveal()->shouldBeCalled();
        $token3->reveal()->shouldBeCalled();
        $token4->reveal()->shouldBeCalled();
        $token5->reveal()->shouldBeCalled();

        $token1->isWinning()->willReturn(false);
        $token2->isWinning()->willReturn(false);
        $token3->isWinning()->willReturn(false);
        $token4->isWinning()->willReturn(false);
        $token5->isWinning()->willReturn(false);

        $this->check(1, 1, 0);
        $this->check(1, 2, 0);
        $this->check(1, 3, 0);
        $this->check(1, 4, 0);
        $this->check(1, 5, 0);

        $this->getResult()->shouldBe(GameResult::LOSE);

        // THEN
        $this->shouldThrow(GameException::alreadyFinished())

        // WHEN
            ->duringCheck(1, 6, 0);
    }

    function it_throws_game_finished_exception_when_checking_after_winning(Board $board, Token $token) {

        // GIVEN
        $board->getToken(1, 1)->willReturn($token);
        $token->isRevealed()->willReturn(false);
        $token->reveal()->shouldBeCalled();
        $token->isWinning()->willReturn(true);

        $this->check(1, 1, 0);

        $this->getResult()->shouldBe(GameResult::WIN);

        // THEN
        $this->shouldThrow(GameException::alreadyFinished())

            // WHEN
            ->duringCheck(1, 2, 0);
    }

    function it_throws_token_already_revealed_exception_when_the_same_spot_checked_two_times(
        Board $board,
        Token $token
    ) {
        // GIVEN
        $board->getToken(1, 1)->willReturn($token);
        $token->isRevealed()->willReturn(false, true);
        $token->reveal()->shouldBeCalled();
        $token->isWinning()->willReturn(false);

        $this->check(1, 1, 0);

        $this->getTries()->shouldBe(1);

        // THEN
        $this->shouldThrow(GameException::tokenAlreadyRevealed())

        // WHEN
            ->duringCheck(1, 1, 0);
    }

    function it_does_not_increment_tries_when_the_same_spot_checked_two_times(Board $board, Token $token) {

        // GIVEN
        $board->getToken(1, 1)->willReturn($token);
        $token->isRevealed()->willReturn(false, true);
        $token->reveal()->shouldBeCalled();
        $token->isWinning()->willReturn(false);

        $this->check(1, 1, 0);
        $this->getTries()->shouldBe(1);

        $this->shouldThrow()

        // WHEN
            ->duringCheck(1, 1, 0);

        // THEN
        $this->getTries()->shouldBe(1);
    }

    function it_returns_time_of_game_start()
    {
        // WHEN
        $this->getStartTime()

        // THEN
            ->shouldBe(0);
    }

    function it_marks_game_with_lose_game_result_when_game_lasts_longer_then_sixty_seconds()
    {
        // WHEN
        $this->check(1,1, 61);

        // THEN
        $this->getResult()->shouldBe(GameResult::LOSE);
    }

    function it_is_possible_to_win_game_if_checked_in_exactly_sixtieth_second(
        Board $board,
        Token $token
    ) {
        // GIVEN
        $board->getToken(1, 1)->willReturn($token);
        $token->isRevealed()->willReturn(false);
        $token->reveal()->shouldBeCalled();
        $token->isWinning()->willReturn(true);

        // WHEN
        $this->check(1,1, 60);

        // THEN
        $this->getResult()->shouldBe(GameResult::WIN);
    }
}
