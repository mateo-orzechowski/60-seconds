<?php

namespace spec\SixtySeconds;

use SixtySeconds\WinningToken;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class WinningTokenSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(WinningToken::class);
    }

    function it_is_winning_token()
    {
        // WHEN
        $this->isWinning()

        // THEN
            ->shouldReturn(true);
    }

    function it_is_not_revealed()
    {
        // WHEN
        $this->isRevealed()

        // THEN
            ->shouldReturn(false);
    }

    function it_reveals_itself()
    {
        // GIVEN
        $this->isRevealed()->shouldReturn(false);

        // WHEN
        $this->reveal();

        // THEN
        $this->isRevealed()->shouldReturn(true);
    }
}
